export interface DiaSelecionado {
  dia?: string;
  checked?: boolean;
  disabled?: boolean
}
