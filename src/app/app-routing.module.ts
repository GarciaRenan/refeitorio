import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './modules/not-found/not-found.component';
import { RefeitorioComponent } from './modules/refeitorio/refeitorio.component';
import { CalendarioComponent } from './modules/calendario/calendario.component';
import { SemanaComponent } from './modules/semana/semana.component';
import { LoginComponent } from './modules/login/login.component';
import { LoginGuard } from './guard/login.guard';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: RefeitorioComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'home/calendario',
    component: CalendarioComponent,
  },
  {
    path: 'home/semana',
    component: SemanaComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
