import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { DiaSelecionado } from 'src/app/interfaces/diaSelecionado';
import { CardapioService } from 'src/app/services/cardapio.service';
import { DataSelecionadaService } from 'src/app/services/dataSelecionada.service';

@Component({
  selector: 'app-semana',
  templateUrl: './semana.component.html',
  styleUrls: ['./semana.component.css'],
})
export class SemanaComponent implements OnInit {
  pipe = new DatePipe('pt-Br');
  minDate = new Date(new Date().setMonth(new Date().getMonth()));
  maxDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
  dataSemana = new Array();

  indeterminate = false;
  labelPosition: 'before' | 'after' = 'after';

  selectedDate: DiaSelecionado = {};

  diaCardapio!: FormGroup;
  items!: FormArray;


  constructor(
    private dataSelecinadaService: DataSelecionadaService,
    private cardapioService: CardapioService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.diaCardapio = this.formBuilder.group({
      items: this.formBuilder.array([])
    });
    //  consome o serviço que seta o dia correto da semana e itera no array para setar corretamente cada dia da semana
    let numSemana = this.dataSelecinadaService.setNumberWeek().nseman;
    let novaData = this.dataSelecinadaService.setNumberWeek().ndate;
    let semana = new Array();
    for (let i = 0; i < 5; i++) {
      semana[i] = i + 1 - 2 * numSemana + numSemana;
      this.dataSemana[i] = new Date(
        new Date(novaData).setDate(new Date(novaData).getDate() + semana[i])
      );

      this.items = this.diaCardapio.get('items') as FormArray;
      this.items.push(this.createItem({dia: this.dataSemana[i], checked: true, disabled: false}));

      this.cardapioService.getByDay('2021-11-02').subscribe((data) => {
        console.log('resultado api', data);
      });
    }
  }

  toggle(event: MatCheckboxChange) {
    // const array = this.diaCardapio.get('DiaRow') as FormArray;
    // for (var row of event.data[0].usersRow) {
    //   array.push(this.CreateDia(row));
    //   //or array.push(this.createUser({username:'test'})
    // }

    console.log('dia clicado', this.diaCardapio.value);
  }

  createItem(data: DiaSelecionado): FormGroup {
    return this.formBuilder.group({
      dia!: data.dia,
      ckecked!: data.checked,
      disabled!: data.disabled
    });
  }
}
