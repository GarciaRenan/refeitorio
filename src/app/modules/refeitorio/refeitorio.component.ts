import { Component, OnInit } from '@angular/core';
import { SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-refeitorio',
  templateUrl: './refeitorio.component.html',
  styleUrls: ['./refeitorio.component.css']
})
export class RefeitorioComponent implements OnInit {

  constructor(private socialAuthService: SocialAuthService) { }

  ngOnInit(): void {
  }

  logOut(): void {
    this.socialAuthService.signOut();
  }
}
