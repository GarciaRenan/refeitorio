import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefeitorioComponent } from './refeitorio.component';

describe('RefeitorioComponent', () => {
  let component: RefeitorioComponent;
  let fixture: ComponentFixture<RefeitorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefeitorioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefeitorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
