import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataSelecionadaService } from 'src/app/services/dataSelecionada.service';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit {

  selectedDate: any;
  dataSelecionada = [];
  minDate = new Date(new Date().setMonth(new Date().getMonth()));
  maxDate = new Date(new Date().setMonth(new Date().getMonth() + 1));

  constructor(private route: Router, private dataSelecionadaService: DataSelecionadaService) { }

  ngOnInit(): void {
  }

  onSelect(event: any){
    this.selectedDate = new Date(event);
    this.dataSelecionadaService.setData(this.selectedDate);
    this.route.navigate(['home/semana']);
  }

}
