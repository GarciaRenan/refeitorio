import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const API = 'http://localhost:4000';

@Injectable({
  providedIn: 'root',
})
export class CardapioService {
  constructor(private httpClient: HttpClient) {}

  // editar atividade
  editar(dia: boolean): Observable<boolean> {
    console.log('teste2');
    return this.httpClient.put<boolean>(
      `${API}/cardapio`, {
        dia
      }
    );
  }

  // busca atividade por id
  getByDay(dia: string): Observable<string> {
    console.log('getByDay');
    return this.httpClient.get<string>(`${API}/cardapio/${dia}`);
  }
}
