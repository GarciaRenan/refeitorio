import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DataSelecionadaService {
  private dataSelecionada = new BehaviorSubject<Date>(new Date());

  constructor() {}

  // Service message commands
  setData(data: string) {
    let newDate = new Date(data);
    this.dataSelecionada.next(newDate);
  }

  // ajusta o dia da semana selecionado no calendário para um dia válido
  // retorna o novo dia da semana em número e data
  setNumberWeek() {
    let newDate = new Date();
    this.getData().subscribe((dia) => {
      newDate = dia;
    });

    let numSemana = Number(newDate.getDay());
    if (numSemana == 0) {
      numSemana += 1;
      newDate = new Date(
        new Date(newDate).setDate(new Date(newDate).getDate() + 1)
      );
    } else if (numSemana == 6) {
      numSemana -= 1;
      newDate = new Date(
        new Date(newDate).setDate(new Date(newDate).getDate() - 1)
      );
    }
    return { nseman: numSemana, ndate: newDate };
  }

  getData() {
    return this.dataSelecionada.asObservable();
  }
}
